use crate::display::Display;
use crate::keyboard::Keyboard;
use std::vec::Vec;

pub struct Cpu {
    pub mem: [u8; 4096], // Memory
    v: [u8; 16],         // Registers
    i: u16,              // Index register
    pc: u16,             // Programm Counter
    stack: [u16; 16],    // stack
    sp: u8,              // Stack pointer
    dt: u8,              // Delay timer
    display: Display,    // Display
    keyboard: Keyboard,  // Keyboard
}

impl Cpu {
    pub fn create() -> Cpu {
        Cpu {
            mem: [0; 4096],
            v: [0; 16],
            i: 0,
            pc: 0,
            stack: [0; 16],
            sp: 0,
            dt: 0,
            display: Display { screen: [0; 2048] },
            keyboard: Keyboard { keypads: 0 },
        }
    }
    pub fn reset(&mut self) {
        self.mem = [0; 4096];
        self.v = [0; 16];
        self.i = 0;
        self.pc = 0x200;
        self.stack = [0; 16];
        self.sp = 0;
        self.dt = 0;
        self.display.screen = [0; 2048];
        self.keyboard.keypads = 0;

        for (i, byte) in fontset.iter().enumerate() {
            self.mem[i + 0x50] = *byte;
        }
    }
    pub fn load_rom(&mut self, rom: &Vec<u8>) -> Option<bool> {
        if rom.len() > 4096 - 0x200 {
            None
        } else {
            for (i, byte) in rom.iter().enumerate() {
                self.mem[i + 0x200] = *byte;
            }
            Some(true)
        }
    }
    pub fn execute_cycle(&mut self) {
        let opcode = Cpu::read_word(self.mem, self.pc);
        self.execute_opcode(opcode);
        self.pc += 2;
    }
    fn read_word(memory: [u8; 4096], index: u16) -> u16 {
        (memory[index as usize] as u16) << 8 | (memory[(index + 1) as usize] as u16)
    }
    fn execute_opcode(&mut self, opcode: u16) {}
}

const fontset: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];
