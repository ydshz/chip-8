extern crate backend;
use std::env::args;
use std::fs;

use backend::cpu::Cpu;

fn main() {
    if args().len() < 2 {
        panic!("Please specify a rom to run.");
    }
    let filename = &args().collect::<Vec<String>>()[1];
    let mut cpu = Cpu::create();
    cpu.reset();
    cpu.load_rom(&fs::read(filename).unwrap()).unwrap();
}
